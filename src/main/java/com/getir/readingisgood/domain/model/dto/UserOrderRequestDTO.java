package com.getir.readingisgood.domain.model.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserOrderRequestDTO implements Serializable {

	private static final long serialVersionUID = -3047400967680260439L;

	@NotNull
	private String userName;
	@NotNull
	private String isbn;
	@NotNull
	private int quantity;
}
