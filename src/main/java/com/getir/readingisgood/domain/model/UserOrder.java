package com.getir.readingisgood.domain.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table (name = "user_order", schema = "reading_is_good")
@EqualsAndHashCode (of = "id")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class UserOrder implements Serializable {

	private static final long serialVersionUID = -5486174080852041915L;

	@Id
	private String id;
	@NotNull
	private String userId;
	@NotNull
	private String bookId;
	@NotNull
	private Integer quantity;
}
