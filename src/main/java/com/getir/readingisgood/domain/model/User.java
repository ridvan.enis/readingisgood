package com.getir.readingisgood.domain.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table (name = "user", schema = "reading_is_good")
@EqualsAndHashCode (of = "id")
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Data
public class User implements Serializable {

	private static final long serialVersionUID = -2525464597020508759L;

	@Id
	private String id;

	@Length (min = 3, max = 50)
	@NotNull
	private String name;
	@Length (min = 3, max = 50)
	@NotNull
	private String surname;
	@Length (min = 5, max = 25)
	@NotNull
	private String userName;
	@Length (min = 6, max = 50)
	@NotNull
	private String email;
	private String password;
}
