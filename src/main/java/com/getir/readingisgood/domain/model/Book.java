package com.getir.readingisgood.domain.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table (name = "book", schema = "reading_is_good")
@EqualsAndHashCode (of = "id")
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Data
public class Book implements Serializable {

	private static final long serialVersionUID = -2528148148361888100L;

	@Id
	@Column
	private String id;
	@NotNull
	@Length (min = 3, max = 255)
	@Column
	private String name;
	@NotNull
	@Length (min = 3, max = 50)
	@Column
	private String isbn;
	@NotNull
	@Length (min = 3, max = 150)
	@Column
	private String author;
	@NotNull
	@Column
	private Integer stock;
}
