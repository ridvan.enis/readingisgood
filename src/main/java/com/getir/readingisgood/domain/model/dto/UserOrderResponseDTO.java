package com.getir.readingisgood.domain.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@SuperBuilder
public class UserOrderResponseDTO {

	private String id;
	private String userName;
	private String bookName;
	private String isbn;
	private Integer quantity;

}
