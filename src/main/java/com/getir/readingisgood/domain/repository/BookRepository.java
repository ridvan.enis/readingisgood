package com.getir.readingisgood.domain.repository;

import java.util.Optional;

import javax.persistence.LockModeType;

import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import com.getir.readingisgood.domain.model.Book;

@Repository
@JaversSpringDataAuditable
public interface BookRepository extends JpaRepository<Book, String> {

	/**
	 * This method search book by isbn number and return.
	 * When an order request come to system we get book information and lock the book record via for the transaction
	 * Until the transaction and other order requests wait. With this way we provide stock consistency
	 * @param isbn
	 * @return
	 */
	@Lock (LockModeType.PESSIMISTIC_WRITE)
	Optional<Book> findByIsbn(String isbn);
}
