package com.getir.readingisgood.domain.repository;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.getir.readingisgood.domain.model.QBook;
import com.getir.readingisgood.domain.model.QUser;
import com.getir.readingisgood.domain.model.QUserOrder;
import com.getir.readingisgood.domain.model.UserOrder;
import com.getir.readingisgood.domain.model.dto.UserOrderResponseDTO;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.JPQLQuery;

@Repository
public class UserOrderRepositoryImpl extends QuerydslRepositorySupport implements CustomUserOrderRepository {

	public UserOrderRepositoryImpl(EntityManager entityManager) {
		super(UserOrder.class);
		setEntityManager(entityManager);
	}

	@Override
	public List<UserOrderResponseDTO> findByUserName(String userName) {
		//@formatter:off
		JPQLQuery<Tuple> selectQuery = from(QUserOrder.userOrder)
				.select(QUserOrder.userOrder.id, QUserOrder.userOrder.quantity, QBook.book.isbn, QBook.book.name, QUser.user.userName)
				.innerJoin(QUser.user)
				.on(QUserOrder.userOrder.userId.eq(QUser.user.id))
				.innerJoin(QBook.book)
				.on(QUserOrder.userOrder.bookId.eq(QBook.book.id))
				.where(QUser.user.userName.eq(userName));
		//@formatter:on

		List<Tuple> tuples = selectQuery.fetch();
		//@formatter:off
		return tuples.stream()
				.map(row -> UserOrderResponseDTO.builder()
						.id(row.get(QUserOrder.userOrder.id))
						.bookName(row.get(QBook.book.name))
						.isbn(row.get(QBook.book.isbn))
						.quantity(row.get(QUserOrder.userOrder.quantity))
						.userName(row.get(QUser.user.userName))
						.build())
				.collect(Collectors.toList());
		//@formatter:on
	}
}
