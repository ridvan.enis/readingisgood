package com.getir.readingisgood.domain.repository;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.getir.readingisgood.domain.model.dto.UserOrderResponseDTO;

@NoRepositoryBean
public interface CustomUserOrderRepository {

	List<UserOrderResponseDTO> findByUserName(String userName);
}
