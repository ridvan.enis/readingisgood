package com.getir.readingisgood.domain.repository;

import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getir.readingisgood.domain.model.UserOrder;

@Repository
@JaversSpringDataAuditable
public interface UserOrderRepository extends CustomUserOrderRepository, JpaRepository<UserOrder, String> {

}
