package com.getir.readingisgood.domain.repository;

import java.util.Optional;

import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getir.readingisgood.domain.model.User;

@Repository
@JaversSpringDataAuditable
public interface UserRepository extends JpaRepository<User, String> {

	Optional<User> findByUserNameOrEmail(String userName, String email);

	Optional<User> findByUserName(String userName);
}
