package com.getir.readingisgood.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private CustomAuthenticationProvider authProvider;

	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth){
		auth.authenticationProvider(authProvider);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.httpBasic().and().authorizeRequests()
				.antMatchers("/", "/csrf", "/v2/api-docs", "/swagger-resources/**", "/swagger-ui.html", "/webjars/**", "/api/v1/user/register" )
				.permitAll()
				.antMatchers("/**").authenticated();
				http.csrf().disable().headers().frameOptions().disable();

				http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

}
