package com.getir.readingisgood.config.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getir.readingisgood.domain.model.User;
import com.getir.readingisgood.service.user.UserService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private UserService userService;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	private ObjectMapper jacksonObjectMapper;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String username = authentication.getName();
		String password = authentication.getCredentials().toString();

		Optional<User> user = userService.findByUserName(username);
		if(!user.isPresent()){
			throw new BadCredentialsException("Username or Password incorrect");
		}
		if(!bCryptPasswordEncoder.matches(password, user.get().getPassword())){
			throw new BadCredentialsException("Username or Password incorrect");
		}

		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = prepareUserPrincipal(authentication, user.get());
		usernamePasswordAuthenticationToken.setDetails(authentication.getDetails());
		return usernamePasswordAuthenticationToken;
	}

	private UsernamePasswordAuthenticationToken prepareUserPrincipal(Authentication authentication, User user) {
		UsernamePasswordAuthenticationToken result;

		UserPrincipal userPrincipal = UserPrincipal.builder().id(user.getId()).email(user.getEmail()).userName(user.getUserName()).build();

		String serializedPrincipal;
		try{
			serializedPrincipal = jacksonObjectMapper.writeValueAsString(userPrincipal);
		} catch (JsonProcessingException e){
			log.error("ERROR : coulndnt convert user principal to json string", e);
			throw new InternalAuthenticationServiceException("couldnt prepare principal object!");
		}
		result = new UsernamePasswordAuthenticationToken(serializedPrincipal, authentication.getCredentials(), new ArrayList<>());
		result.setDetails(authentication.getDetails());
		return result;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
