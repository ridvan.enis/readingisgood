package com.getir.readingisgood.config.flyway;

import java.nio.charset.StandardCharsets;

import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.Location;
import org.flywaydb.core.api.configuration.ClassicConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FlywayConfiguration {

	@Autowired
	private DataSource dataSource;

	@Bean (initMethod = "migrate")
	public Flyway flyway() {
		return new Flyway(configure());
	}

	private ClassicConfiguration configure() {
		ClassicConfiguration flywayConfiguration = new ClassicConfiguration();
		flywayConfiguration.setEncoding(StandardCharsets.UTF_8);
		flywayConfiguration.setBaselineOnMigrate(true);
		flywayConfiguration.setOutOfOrder(true);
		Location location = new Location("classpath:/db/migration");
		flywayConfiguration.setLocations(location);
		flywayConfiguration.setDataSource(dataSource);
		flywayConfiguration.setTable("migration");
		flywayConfiguration.setSqlMigrationPrefix("V");
		flywayConfiguration.setSqlMigrationSuffixes(".sql");
		return flywayConfiguration;
	}
}
