package com.getir.readingisgood.service.order;

import java.util.List;

import com.getir.readingisgood.domain.model.UserOrder;
import com.getir.readingisgood.domain.model.dto.UserOrderRequestDTO;
import com.getir.readingisgood.domain.model.dto.UserOrderResponseDTO;

public interface UserOrderService {

	UserOrder makeOrder(UserOrderRequestDTO requestDTO);

	List<UserOrderResponseDTO> getOrderByUserName(String userName);
}
