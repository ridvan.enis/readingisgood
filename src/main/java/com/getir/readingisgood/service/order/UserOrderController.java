package com.getir.readingisgood.service.order;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.getir.readingisgood.config.security.UserPrincipal;
import com.getir.readingisgood.domain.model.UserOrder;
import com.getir.readingisgood.domain.model.dto.UserOrderRequestDTO;
import com.getir.readingisgood.domain.model.dto.UserOrderResponseDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping ("/api/v1/order")
@Api (value = "User Order CRUD Operations")
public class UserOrderController {

	private UserOrderService userOrderService;

	public UserOrderController(UserOrderService userOrderService) {
		this.userOrderService = userOrderService;
	}

	@ApiOperation (value = "Create new user order")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 200, message = "Successfully created"),
			@ApiResponse(code = 400, message = "Error occured")
	})
	@PostMapping ("/")
	public ResponseEntity<UserOrder> makeOrder(@RequestBody @Valid UserOrderRequestDTO userOrderRequestDTO, @AuthenticationPrincipal UserPrincipal userPrincipal) {
		UserOrder userOrder = userOrderService.makeOrder(userOrderRequestDTO);
		return ResponseEntity.ok(userOrder);
	}

	@ApiOperation (value = "Get all orders by username")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 200, message = "Successfully listed"),
			@ApiResponse(code = 400, message = "Error occured")
	})
	@GetMapping ("/username/{username}")
	public ResponseEntity<List<UserOrderResponseDTO> > getOrdersByUserName(@PathVariable("username") String userName, @AuthenticationPrincipal AuthenticationPrincipal principal) {
		List<UserOrderResponseDTO> userOrders = userOrderService.getOrderByUserName(userName);
		return ResponseEntity.ok(userOrders);
	}
}
