package com.getir.readingisgood.service.order.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.getir.readingisgood.domain.model.Book;
import com.getir.readingisgood.domain.model.User;
import com.getir.readingisgood.domain.model.UserOrder;
import com.getir.readingisgood.domain.model.dto.UserOrderRequestDTO;
import com.getir.readingisgood.domain.model.dto.UserOrderResponseDTO;
import com.getir.readingisgood.domain.repository.UserOrderRepository;
import com.getir.readingisgood.exception.RestResourceNotFoundException;
import com.getir.readingisgood.exception.RestResourceStockNotAvailableException;
import com.getir.readingisgood.service.book.BookService;
import com.getir.readingisgood.service.order.UserOrderService;
import com.getir.readingisgood.service.user.UserService;

import lombok.extern.slf4j.Slf4j;

/**
 * This is a facade service for managing orders of users
 */
@Service
@Slf4j
public class UserOrderServiceImpl implements UserOrderService {

	private UserService userService;
	private BookService bookService;
	private UserOrderRepository userOrderRepository;

	public UserOrderServiceImpl(UserService userService, BookService bookService, UserOrderRepository userOrderRepository) {
		this.userService = userService;
		this.bookService = bookService;
		this.userOrderRepository = userOrderRepository;
	}

	@Override
	@Transactional
	public UserOrder makeOrder(UserOrderRequestDTO requestDTO) {
		User user = getUserInfo(requestDTO.getUserName());
		Book book = getBookInfo(requestDTO.getIsbn());
		checkStock(book.getStock(), requestDTO.getQuantity());

		bookService.updateBookStock(book, requestDTO.getQuantity());

		return createOrder(user.getId(), book.getId(), requestDTO.getQuantity());
	}

	private User getUserInfo(String username) {
		Optional<User> user = userService.findByUserName(username);
		if(!user.isPresent()){
			throw RestResourceNotFoundException.builder().message("User not found").build();
		}
		return user.get();
	}

	private Book getBookInfo(String isbn) {
		Optional<Book> book = bookService.findByIsbn(isbn);
		if(!book.isPresent()){
			throw RestResourceNotFoundException.builder().message("Book not found").build();
		}
		return book.get();
	}

	private void checkStock(Integer bookStock, int quantity) {
		if(bookStock.intValue() - quantity < 0){
			throw RestResourceStockNotAvailableException.builder().message("stock not available").build();
		}
	}

	private UserOrder createOrder(String userId, String bookId, int quantity) {
		UserOrder userOrder = UserOrder.builder().id(UUID.randomUUID().toString()).bookId(bookId).userId(userId).quantity(quantity).build();
		return userOrderRepository.save(userOrder);
	}

	@Override
	public List<UserOrderResponseDTO> getOrderByUserName(String userName) {
		return userOrderRepository.findByUserName(userName);
	}
}
