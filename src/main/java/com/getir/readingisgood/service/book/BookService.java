package com.getir.readingisgood.service.book;

import java.util.Optional;

import com.getir.readingisgood.domain.model.Book;

public interface BookService {

	Optional<Book> findByIsbn(String isbn);

	void updateBookStock(Book book, int quantity);
}
