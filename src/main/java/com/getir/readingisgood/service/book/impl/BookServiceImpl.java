package com.getir.readingisgood.service.book.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.getir.readingisgood.domain.model.Book;
import com.getir.readingisgood.domain.repository.BookRepository;
import com.getir.readingisgood.service.book.BookService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BookServiceImpl implements BookService {

	private BookRepository bookRepository;

	public BookServiceImpl(BookRepository bookRepository) {
		this.bookRepository = bookRepository;
	}

	@Override
	public Optional<Book> findByIsbn(String isbn) {
		return bookRepository.findByIsbn(isbn);
	}

	@Override
	@Transactional
	public void updateBookStock(Book book, int quantity) {
		book.setStock(book.getStock() - quantity);
		bookRepository.save(book);
	}
}
