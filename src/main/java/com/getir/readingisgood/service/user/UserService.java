package com.getir.readingisgood.service.user;

import java.util.Optional;

import com.getir.readingisgood.domain.model.User;

public interface UserService {

	User create(User user);

	Optional<User> findByUserName(String userName);
}
