package com.getir.readingisgood.service.user;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.getir.readingisgood.domain.model.User;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping ("/api/v1/user")
@Api (value = "User CRUD Operations")
public class UserController {

	private UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@ApiOperation (value = "Create new suer")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 200, message = "Successfully created"),
			@ApiResponse(code = 400, message = "Error occured")
	})
	@PostMapping ("/register")
	public ResponseEntity<User> register(@RequestBody @Valid User user) {
		User savedUser = userService.create(user);
		return ResponseEntity.ok(savedUser);
	}
}
