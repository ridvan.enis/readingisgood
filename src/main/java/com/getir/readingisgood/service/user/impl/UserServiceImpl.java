package com.getir.readingisgood.service.user.impl;

import java.util.Optional;
import java.util.UUID;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.getir.readingisgood.domain.model.User;
import com.getir.readingisgood.domain.repository.UserRepository;
import com.getir.readingisgood.exception.RestUserAlreadyExistsException;
import com.getir.readingisgood.service.user.UserService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepository = userRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	public User create(User user) {
		checkUserIsExists(user);
		//system set new id for create operation
		user.setId(UUID.randomUUID().toString());
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		return userRepository.save(user);
	}

	private void checkUserIsExists(User user) {
		Optional<User> userOptional = userRepository.findByUserNameOrEmail(user.getUserName(), user.getEmail());
		if(userOptional.isPresent()){
			throw RestUserAlreadyExistsException.builder().message("User Already Exist").build();
		}
	}

	@Override
	public Optional<User> findByUserName(String userName) {
		return userRepository.findByUserName(userName);
	}
}
