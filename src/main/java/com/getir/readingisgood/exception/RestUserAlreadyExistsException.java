package com.getir.readingisgood.exception;

import lombok.Builder;

public class RestUserAlreadyExistsException extends RuntimeException{

	private static final long serialVersionUID = 1903700374365830171L;

	@Builder
	public RestUserAlreadyExistsException(String message) {
		super(message);
	}
}
