package com.getir.readingisgood.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.getir.readingisgood.exception.RestResourceNotFoundException;
import com.getir.readingisgood.exception.RestResourceStockNotAvailableException;
import com.getir.readingisgood.exception.RestUserAlreadyExistsException;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class RestExceptionHandler {

	/**
	 * Catch any unknown error/exception and provide readable response to end-user.
	 *
	 * @param ex Throwable object for error/exception.
	 * @return the error response DTO {@link ErrorResponse}
	 */
	@ResponseStatus (HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler (Throwable.class)
	@ResponseBody
	public ErrorResponse handleThrowable(final Throwable ex) {
		log.error("Unexpected error", ex);
		return ErrorResponse.builder().status("INTERNAL_SERVER_ERROR").message("An unexpected internal server error occurred").build();

	}

	/**
	 * Catch argument not valid error/exception and provide readable response to end-user.
	 *
	 * @param ex MethodArgumentNotValidException object for error/exception.
	 * @return the error response DTO {@link ErrorResponse}
	 */
	@ResponseStatus (HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler (MethodArgumentNotValidException.class)
	@ResponseBody
	public ErrorResponse handleMethodArgumentNotValidException(final Throwable ex) {
		log.error("Unexpected error", ex);
		return ErrorResponse.builder().status("INTERNAL_SERVER_ERROR").message(ex.getMessage()).build();

	}

	/**
	 * Catch if the user already registered in the system
	 * @param ex RestUserAlreadyExistsException object for exception
	 * @return the error response DTO {@link ErrorResponse}
	 */
	@ExceptionHandler (RestUserAlreadyExistsException.class)
	@ResponseStatus (HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorResponse handleUserAlreadyExistsException(Exception ex) {
		return ErrorResponse.builder().message(ex.getMessage()).status("BAD_REQUEST").build();
	}

	/**
	 * Catch resource not found in the system
	 * @param ex RestResourceNotFoundException object for exception
	 * @return the error response DTO {@link ErrorResponse}
	 */
	@ExceptionHandler (RestResourceNotFoundException.class)
	@ResponseStatus (HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorResponse handleRestourceNotFoundException(Exception ex) {
		return ErrorResponse.builder().message(ex.getMessage()).status("REST_RESOURCE_NOT_FOUND").build();
	}

	/**
	 * Catch stock not available error in the system
	 * @param ex RestResourceStockNotAvailableException object for exception
	 * @return the error response DTO {@link ErrorResponse}
	 */
	@ExceptionHandler (RestResourceStockNotAvailableException.class)
	@ResponseStatus (HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorResponse handleRestStockNotAvailableException(Exception ex) {
		return ErrorResponse.builder().message(ex.getMessage()).status("REST_RESOURCE_NOT_FOUND").build();
	}
}
