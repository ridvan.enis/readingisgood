package com.getir.readingisgood.exception;

import lombok.Builder;

public class RestResourceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -7115736728164480974L;

	@Builder
	public RestResourceNotFoundException(String message) {
		super(message);
	}
}
