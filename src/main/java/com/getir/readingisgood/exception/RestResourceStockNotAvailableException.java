package com.getir.readingisgood.exception;

import lombok.Builder;

public class RestResourceStockNotAvailableException extends RuntimeException {

	private static final long serialVersionUID = 745753174780235673L;

	@Builder
	public RestResourceStockNotAvailableException(String message) {
		super(message);
	}
}
