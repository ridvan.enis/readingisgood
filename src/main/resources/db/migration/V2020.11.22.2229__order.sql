CREATE TABLE IF NOT EXISTS reading_is_good.user_order(
id char(36) primary key,
user_id varchar (255),
book_id varchar (50),
status int,
quantity int8
);