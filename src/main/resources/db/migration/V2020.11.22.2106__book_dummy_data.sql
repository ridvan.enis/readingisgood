
-- insert some dumy data for book table
INSERT INTO reading_is_good.book(id, name, isbn, author, stock) values ('e01f7006-708b-4b2e-b6ae-cde71e30bb25', 'Clean Code ', '9780132350884', 'Uncle Bob Martin', 130);
INSERT INTO reading_is_good.book(id, name, isbn, author, stock) values ('a8e94696-5181-41f3-8928-1c5a5c9f2edf', 'Refactoring', '9780134757599', 'Martin Fowler', 12 );
INSERT INTO reading_is_good.book(id, name, isbn, author, stock) values ('6d894641-5e30-4872-856f-7f941d5b66d5', 'Design Patterns: Elements of Reusable Object-Oriented Software', '0201633612', 'Erich Gamma', 34);
INSERT INTO reading_is_good.book(id, name, isbn, author, stock) values ('ddee1e3c-d581-490f-96b3-04a8343bbcae', 'Pattern-Oriented Software Architecture Volume 1: A System of Patterns', '0471958697', 'Frank Buschmann', 20);
INSERT INTO reading_is_good.book(id, name, isbn, author, stock) values ('bbf551bd-4e11-44cb-8513-baf6d4dd4ef3', 'Building Microservices: Designing Fine-Grained Systems', '1491950358', 'Sam Newman', 3);
INSERT INTO reading_is_good.book(id, name, isbn, author, stock) values ('7754f652-cf2d-4f7a-bc40-0d507c41ef59', 'Fundamentals of Software Architecture: An Engineering Approach', '1492043451', 'Mark Richards', 67);
INSERT INTO reading_is_good.book(id, name, isbn, author, stock) values ('a0b83a10-92b9-4b6e-87ee-57ad0c0e91da', 'Enterprise Integration Patterns: Designing, Building, and Deploying Messaging Solutions', '9780321200686', 'Gregor Hohpe', 0);