CREATE TABLE IF NOT EXISTS reading_is_good.book(
id char(36) primary key,
name varchar (255),
isbn varchar (50),
author varchar(150),
stock int8
)