CREATE TABLE IF NOT EXISTS reading_is_good.user(
id char(36) primary key,
name varchar (50),
surname varchar (50),
user_name varchar(25),
email varchar(50),
password varchar (100)
);

insert into reading_is_good.user(id, name, surname, user_name, email, password) values ('6fda9a00-0ddb-4a14-811a-02e0cd91ac7b', 'Ridvan', 'ENIS', 'renis', 'ridvan.enis@gmail.com', '$2a$10$m1eoRFBllRnucOx6zF/X4.wez7.ht10whnu0JWbnRFaF9HqMjDJwO' );